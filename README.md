# ItunesAppStoreListing
[![pipeline status](https://gitlab.com/xhiyu/itunesappstorelisting/badges/master/pipeline.svg)](https://gitlab.com/xhiyu/itunesappstorelisting/commits/master)
[![coverage report](https://gitlab.com/xhiyu/itunesappstorelisting/badges/master/coverage.svg)](https://gitlab.com/xhiyu/itunesappstorelisting/commits/master)


This project is SPA to listing the top free and top grossing applications in Hong Kong iTunes App Store.


## Preview
https://itunes.daydayfun.com/

![](gif-small.gif)


## Tech Stack
- FrontEnd
    - React, Redux, Saga, Styled-components
- BackEnd
    - Koa
- Hosting
    - k8s by GKE
- DNS management
    - Cloudflare

## Highlight
- styled-components 
- redux-saga to handle asynchronous things
- serviceworker to cache the static file
- added animation for lazy loading
- toast message when network error
- CI/CD in Gitlab
  - Auto build static and docker image
  - Rolling update the application 


## To be improved & anti-pattern
- More test cases to improve the code coverage
- optimize the bundle size
- seo not-friendly (best practice: apply SSR)
- apply helm chart

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) > 8 or yarn(optional).

```sh
# start react dev-server
$ git clone git@gitlab.com:xhiyu/itunesappstorelisting.git
$ cd itunesappstorelisting
$ npm install # or yarn install 
$ npm start # or yarn start

# start api proxy server
$ npm run server
```

## Deploy to GKE
0. First of all, you should have a gke cluster
1. Read this [page](https://cloud.google.com/kubernetes-engine/docs/how-to/stateless-apps) to create build and push the image to google container registry

2. Read this [page](https://cloud.google.com/kubernetes-engine/docs/how-to/exposing-apps#creating_a_service_of_type_nodeport) to expose your application to public

3. If you prefer to use NodePort service and access the endpoint directly, don't forget to set one of the VM ip to static and set a firewall rule to whitelist the node port
```sh
# Step 1
# make sure: your console already connected the gke cluster and updated your image registry path(line 19)
$ kubectl apply -f ./k8s-deployment.yaml

# Step 2
# create a nodePort service for this dpeloyment (if you want to access the endpoint by vm_ip:nodeport, don't forget to add a [firewall-rules](https://cloud.google.com/kubernetes-engine/docs/how-to/exposing-apps#creating_a_service_of_type_nodeport))
$ kubectl apply -f ./k8s-service.yaml

# Step 3 (Option)
# if you want a GCP http load balaner
$ kubectl apply -f k8s-service-ingress.yaml
```


## Testing
Runs the test watcher in an interactive mode.
You may need to install `brew install watchman` if for mac os, [issue](https://github.com/facebook/jest/issues/1767)

```sh
$ npm test  # or yarn test
```
