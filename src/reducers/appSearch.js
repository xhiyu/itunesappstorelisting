import { SEARCH_APP } from 'constants/ActionTypes'

const searchApp = (state = { keyword: '' }, action) => {
	switch (action.type) {
		case SEARCH_APP:
			return { ...state, keyword: action.keyword }
		default:
			return state
	}
}

export default searchApp
