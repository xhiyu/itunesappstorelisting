import { combineReducers } from 'redux'

import appSearch from './appSearch'
import appListing from './appListing'
import appRecommendation from './appRecommendation'
import notification from './notification'

const rootReducer = combineReducers({
	appListing,
	appRecommendation,
	notification,
	appSearch,
})

export default rootReducer
