import { put, fork, select, takeLatest, takeEvery } from 'redux-saga/effects'
import axios from 'axios'

import { getAppListingPage, getAppListingappDB } from 'selectors/appListing'

import {
	INCREASE_TOP_FREE_LIST_PAGE,
	FETCH_TOP_FREE_APP_REQUEST,
	FETCH_TOP_FREE_ALL_APP_DONE,
	FETCH_TOP_FREE_APP_DONE,
	FETCH_TOP_FREE_APP_DETAIL_REQUEST,
	FETCH_TOP_FREE_APP_DETAIL_DONE,
	SCROLL_TOP_FREE_APP_END,
	SHOW_NETWORK_ERROR,
} from 'constants/ActionTypes'

const delay = (ms) => new Promise((res) => setTimeout(res, ms))

export function* fetchFreeTopApp() {
	yield put({ type: INCREASE_TOP_FREE_LIST_PAGE })

	const page = yield select(getAppListingPage)

	// stop the flow if the scrolled to 100th app
	if (page > 10) {
		yield put({ type: SCROLL_TOP_FREE_APP_END })
		return
	}

	if (page === 1) {
		const error = null
		try {
			const res = yield axios.get(`/api/topfreeapplications.json`)
			const payload = res.data
			yield put({ type: FETCH_TOP_FREE_ALL_APP_DONE, error, payload })
		} catch (err) {
			yield put({ type: SHOW_NETWORK_ERROR, hasError: true })
		}
	}

	// save all top 100 free app, then slice the app to the app listing which are wanted to show
	const appDB = yield select(getAppListingappDB)

	const selectedApp = appDB.slice((page - 1) * 10, page * 10)
	const payload = selectedApp.map((item) => ({
		id: item.id.attributes['im:id'],
		category: item.category.attributes.label,
		image: item['im:image'][1].label,
		title: item['im:name'].label,
		summary: item.summary.label,
		artist: item['im:artist'].label,
		isLoading: true,
	}))

	// Add some delay to make the loading animation more smooth
	yield delay(100)

	yield put({ type: FETCH_TOP_FREE_APP_DONE, payload })
}

export function* fetchFreeTopAppDetail({ id, appIndex }) {
	let payload = {}
	let res = {}

	try {
		res = yield axios.get(`/api/lookup/${id}.json`)
	} catch (err) {
		yield put({ type: SHOW_NETWORK_ERROR, hasError: true })
	}

	if (res.status === 200) {
		payload = res.data
	} else {
		yield put({ type: SHOW_NETWORK_ERROR, hasError: true })
	}

	// Add some delay to make the loading animation more smooth
	yield delay(500)
	yield put({ type: FETCH_TOP_FREE_APP_DETAIL_DONE, payload, appIndex })
}

export default function*() {
	yield fork(takeLatest, FETCH_TOP_FREE_APP_REQUEST, fetchFreeTopApp)
	yield fork(takeEvery, FETCH_TOP_FREE_APP_DETAIL_REQUEST, fetchFreeTopAppDetail)
}
