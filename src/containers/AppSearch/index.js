import { connect } from 'react-redux'
import { searchApp } from 'actions/appSearch'

import AppSearch from './AppSearch'

const mapStateToProps = (state) => {
	return {
		hasSearchKeyword: Boolean(state.appSearch.keyword),
	}
}

const mapDispatchToProps = {
	searchApp,
}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(AppSearch)
