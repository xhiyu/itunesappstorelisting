import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { ToastContainer, toast } from 'react-toastify'

const DEFAULT_MESSAGE = 'Something went wrong at your network. Please try again.'

class Toast extends PureComponent {
	static propTypes = {
		isShow: PropTypes.bool,
	}

	static defaultProps = {
		isShow: false,
	}

	render() {
		if (this.props.isShow) {
			toast.error(DEFAULT_MESSAGE, {
				position: 'top-center',
				autoClose: false,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
			})
		}

		return (
			<ToastContainer
				position="top-center"
				autoClose={false}
				newestOnTop={false}
				closeOnClick
				rtl={false}
				pauseOnVisibilityChange
				draggable
			/>
		)
	}
}

export default Toast
