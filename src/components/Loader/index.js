import React from 'react'
import styled from 'styled-components'

const LoaderWraper = styled.div`
	margin: auto;
	padding-top: ${(props) => props.theme.spaceS};
	font-size: ${(props) => props.theme.fontSizeL};
`

const Loader = () => (
	<LoaderWraper>
		<i className="fa fa-spinner fa-spin" />
	</LoaderWraper>
)

export default Loader
