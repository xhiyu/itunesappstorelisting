FROM keymetrics/pm2:10-alpine

WORKDIR /app/

COPY . .

# https://vsupalov.com/docker-env-vars/

ARG BUILD_NO=0
ENV buildVersion=${BUILD_NO}
RUN env

RUN npm install -g yarn

RUN yarn install --production --frozen-lockfile

RUN ls node_modules

EXPOSE 9000

CMD [ "pm2-runtime", "start", "pm2.json" ]

